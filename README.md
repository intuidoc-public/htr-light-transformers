# Very Light Transformer (VLT) Architecture

COMING SOON !
I just have to take a one-week break, and I will start cleaning, updating, and posting the code here!

This repository will contain the source code regarding the Very Light Transformer architecture, implemented using Pytorch.

It will include the following:
- Installation instructions
- The architecture source code
- Training instructions, with examples
- Weights of a trained model
- Instructions for evaluating the model, with examples
